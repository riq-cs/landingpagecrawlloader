# weekly_scans_gainsight_update.1-7.py v1.7
# written by: Phil Cowger
# last updated: udpated 10/9/2017
# This script grabs the previous weeks success landing page scan volumes, exports
# them to a formatted .csv, and uploads that .csv to Gainsigh.  This script is intended
# to be run, by cronjob every Monday morning.
# changelog: v1.5 added retry logic to stop after 5 tries
# changelog: v1.6 took out common.py progress bar
# changelog: v1.7 changed the retry logic, moved the db cnx/cursor to the thread level

import datetime
import mysql.connector
import json
import collections
import time
from datetime import date, time
import dateutil.parser
import csv
import requests
import threading
from threading import Thread
import Queue
import sys
from requests_toolbelt.multipart.encoder import MultipartEncoder
import os

WS_LIST = []
ed = datetime.date.today()
sd = ed - datetime.timedelta(days=7)
R_CON = {
    's_date': str(sd.strftime('%Y-%m-%d')),
    'e_date': str(ed.strftime('%Y-%m-%d'))
}
QUEUE_LOCK = threading.Lock()
QUERY_CON = {
    'query_total': 0,
    'count': 0
}
CONFIG = {
    'user':'linkco_readonly',
    'password': 'l1nk0',
    'host': 'internal-usw2-mysql-slaves-698196514.us-west-2.elb.amazonaws.com',
    'database': 'linkco_prod',
    'port': '3306',
    'connection_timeout': 30
}

def get_workspaces():
# MySQL Query that grabs all current, non-paused, non-demo workspaces with salesforce Id's
# that have landing pages turned on.
# returns list of workspace name, workspace Id, and salesforce Id
    print "- Getting Workspaces -"
    for x in range(10):
        print " - CNX Attempt # " + str(x)
        try:
            CNX = mysql.connector.connect(**CONFIG)
            cursor = cursor = CNX.cursor(dictionary=True)
            query = ("""
                SELECT a.name, a.workspaceId, a.salesForceID
                FROM workspaces AS a
                  JOIN workspacefeatures AS b on a.workspaceId = b.workspaceId
                WHERE a.statusCode = 'Y' AND a.paused = 0 AND a.salesForceID != '' AND a.demo != 1
                AND (b.hasLandingPages IS NULL OR b.hasLandingPages = 1)"""
            )
            cursor.execute(query)
            workspaces = []
            for x in cursor:
                ws = {}
                ws['ws_name'] = x['name']
                ws['ws_id'] = x['workspaceId']
                ws['salesforce_id'] = x['salesForceID']
                workspaces.append(ws)
            cursor.close()
            CNX.close()
            print json.dumps(workspaces, indent=4)
            return workspaces
        except Exception as e:
            if x == 10:
                print " - ending after " + str(x) + " attempts - " + str(e)
                break
            print " - attempt # " + str(x) + " - " + str(e)
            continue
        break

def get_weekly_crawls(ws, T_CNX, cursor):
# MySQL Query that gets a weekly summed count of successful landing page scans for
# standard, residential, and cellular proxies.
# returns dict with workspace name & ID, salesforce Id, date (last date of the week
# being summed for gainsight purposes), residential scan sums, standard scan sums,
# and cellular scan sums
    for x in range(10):
        try:
            query = ("""
                SELECT d.name, d.salesforceId, a.workspaceId, a.startDate,
                    (SELECT SUM(a1.crawledPages)
                    FROM crawlproxydailysummary AS a1
                        JOIN proxies AS b1 ON a1.proxyId = b1.proxyId
                        JOIN metrocodes AS c1 ON b1.metroId = c1.metroId
                    WHERE a1.workspaceId = %(x)s AND a1.startDate >= '%(y)s' AND a1.startDate < '%(z)s' AND c1.proxytype= 'C'
                    GROUP BY a1.workspaceId) AS cellular_scans,
                    (SELECT SUM(a2.crawledPages)
                    FROM crawlproxydailysummary AS a2
                        JOIN proxies AS b2 ON a2.proxyId = b2.proxyId
                        JOIN metrocodes AS c2 ON b2.metroId = c2.metroId
                    WHERE a2.workspaceId = %(x)s AND a2.startDate >= '%(y)s' AND a2.startDate < '%(z)s' AND c2.proxytype= 'S'
                    GROUP BY a2.workspaceId) AS standard_scans,
                    (SELECT SUM(a3.crawledPages)
                    FROM crawlproxydailysummary AS a3
                        JOIN proxies AS b3 ON a3.proxyId = b3.proxyId
                        JOIN metrocodes AS c3 ON b3.metroId = c3.metroId
                    WHERE a3.workspaceId = %(x)s AND a3.startDate >= '%(y)s' AND a3.startDate < '%(z)s' AND c3.proxytype= 'R'
                    GROUP BY a3.workspaceId) AS residential_scans
                FROM crawlproxydailysummary AS a
                JOIN workspaces AS d ON a.workspaceId = d.workspaceId
                WHERE a.workspaceId = %(x)s AND a.startDate >= '%(y)s' AND a.startDate < '%(z)s'
                GROUP BY a.workspaceId""" % {
                        'x':str(ws['ws_id']),
                        'y':str(ws['start_date']),
                        'z':str(ws['end_date'])}
                )
            cursor.execute(query)
            data = cursor.fetchall()
        except Exception as e:
            if x == 10:
                print " - ending after " + str(x) + " attempts - " + str(e)
                break
            print " - attempt # " + str(x) + " - " + str(e)
            continue
        break
    last_day = datetime.datetime.strptime(ws['end_date'], '%Y-%m-%d') - datetime.timedelta(days=1)
    if data:
        for x in data:
            dcr = {}
            dcr['ws_name'] = str(ws['ws_name'])
            dcr['salesforce_id'] = str(ws['salesforce_id'])
            dcr['ws_id'] = str(ws['ws_id'])
            dcr['date'] = last_day.strftime('%Y-%m-%d')
            if x['cellular_scans']:
                dcr['cellular_scans'] = str(x['cellular_scans'])
            else:
                dcr['cellular_scans'] = '0'
            if x['standard_scans']:
                dcr['standard_scans'] = str(x['standard_scans'])
            else:
                dcr['standard_scans'] = '0'
            if x['residential_scans']:
                dcr['residential_scans'] = str(x['residential_scans'])
            else:
                dcr['residential_scans'] = '0'
            return dcr
    else:
        dcr = {}
        dcr['ws_name'] = str(ws['ws_name'])
        dcr['salesforce_id'] = str(ws['salesforce_id'])
        dcr['ws_id'] = str(ws['ws_id'])
        dcr['date'] = last_day.strftime('%Y-%m-%d')
        dcr['cellular_scans'] = '0'
        dcr['standard_scans'] = '0'
        dcr['residential_scans'] = '0'
        return dcr

def daily_sequencer():
# This function grabs all the relevant workspaces, creates the threads (for speed of runtime),
# and populates the queue.
    workspaces = get_workspaces()
    print " -- Getting " + str(len(workspaces)) + " Workspaces --"
    QUERY_CON["query_total"] = int(len(workspaces))
    print str(QUERY_CON["count"]) + "/" + str(QUERY_CON["query_total"])
    start_day = datetime.datetime.strptime(R_CON['s_date'], '%Y-%m-%d')
    end_day = datetime.datetime.strptime(R_CON['e_date'], '%Y-%m-%d')
    ws_list = []
    for ws in workspaces:
        week = {}
        week['start_date'] = start_day.strftime('%Y-%m-%d')
        week['end_date'] = end_day.strftime('%Y-%m-%d')
        week['ws_id'] = ws['ws_id']
        week['ws_name'] = ws['ws_name']
        week['salesforce_id'] = ws['salesforce_id']
        ws_list.append(week)
    queue = Queue.Queue()
    for i in range(10):
        for x in range(100):
            try:
                print " - getting connection: try # " + str(x) + " for T# " + str(i)
                T_CNX = mysql.connector.connect(**CONFIG)
                cursor = T_CNX.cursor(dictionary=True)
            except mysql.connector.Error as err:
                print("   -A# " + str(x) + " -T#" + str(i) + "  -Error: " + str(err))
                continue
            print " - connection success"
            break
        thread = threading.Thread(target=ws_threader, args=(i, queue, T_CNX, cursor))
        thread.setDaemon(True)
        thread.start()
    for ws in ws_list:
        queue.put(ws)
    queue.join()

def ws_threader(i, queue, T_CNX, cursor):
# This is the threading controller function that loops through the workspace queue
# until all workspaces have been processed.
    while True:
        if not queue.empty():
            QUEUE_LOCK.acquire()
            ws = queue.get()
            QUEUE_LOCK.release()
            weekly_posts = get_weekly_crawls(ws, T_CNX, cursor)
            WS_LIST.append(weekly_posts)
            QUERY_CON["count"]+=1
            print str(QUERY_CON["count"]) + "/" + str(QUERY_CON["query_total"])
            queue.task_done()

def export_csv(WS_LIST):
# This function exports the completed scan summaries to a .csv file formatted for
# upload to gainsight.
    ws_parsed = json.loads(json.dumps(WS_LIST))
    json.dumps(ws_parsed, indent=4)
    f = csv.writer(open("weekly_scans_gainsight_update.csv", "wb+"))
    f.writerow(["Account ID", "Date", "Regular Scans", "Cellular Scans", "Residential Scans"])
    for x in ws_parsed:
        f.writerow([x["salesforce_id"],
            x["date"],
            x["standard_scans"],
            x["cellular_scans"],
            x["residential_scans"]])

def gainsight_api():
# This function uploads the previously created scan summary .csv to Gainsight
    m = MultipartEncoder(
        fields={
            "jobId":"6b3c7170-4e8a-490b-aec2-525bb3477280",
            "file": ("weekly_scans_gainsight_update.csv", open("weekly_scans_gainsight_update.csv", "rb"), "text/plain")
        }
    )
    url = "https://app.gainsight.com/v1.0/admin/connector/job/bulkimport"
    headers = {
        "Content-Type": m.content_type,
        "loginName": "chris.duckers@riskiq.net",
        "appOrgId": "00Di0000000JuXdEAK",
        "accessKey": "4dee9b32-0f87-4596-88d4-44cba521e638",
    }
    try:
        response = requests.post(url,data=m,headers=headers)
        print response.status_code
        parsed_result = json.loads(response.text)
        print json.dumps(parsed_result, indent=4)
    except Exception as e:
        print "Gainsight API Upload Error: "
        print str(e)

def main():
# Main function call
    try:
    #removes any previous/existing .csv files
        os.remove('weekly_scans_gainsight_update.csv')
    except Exception as e:
        print str(e)
    daily_sequencer()
    export_csv(WS_LIST)
    #gainsight_api()

main()
